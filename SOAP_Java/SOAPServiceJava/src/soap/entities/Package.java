package soap.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "package")
public class Package implements Serializable {
    @Id
    private int id;
    @OneToOne
    private User sender;
    @OneToOne
    private User reciever;
    private String name;
    private String description;
    private String senderCity;
    private String destinationCity;
    private Boolean tracking;
    @OneToMany
    private List<Halt> route;

    public Package() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReciever() {
        return reciever;
    }

    public void setReciever(User reciever) {
        this.reciever = reciever;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public Boolean getTracking() {
        return tracking;
    }

    public void setTracking(Boolean tracking) {
        this.tracking = tracking;
    }

    public List<Halt> getRoute() {
        return route;
    }

    public void setRoute(List<Halt> route) {
        this.route = route;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", sender=" + sender +
                ", reciever=" + reciever +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCity='" + senderCity + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", tracking=" + tracking +
                ", route=" + route +
                '}';
    }
}
