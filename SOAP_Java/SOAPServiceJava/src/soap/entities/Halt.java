package soap.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "halt")
public class Halt implements Serializable {
    @Id
    private int id;
    private String city;
    private Date date;

    public Halt() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Halt{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", date=" + date +
                '}';
    }
}
