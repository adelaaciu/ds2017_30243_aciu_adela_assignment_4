package soap.service;

import soap.dao.PackageDao;
import soap.entities.Package;
import soap.entities.PackageContainer;
import soap.entities.User;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;
@WebService
@SOAPBinding(style= SOAPBinding.Style.RPC)
public class PackageService implements  IPackageService{
    public PackageContainer getPackages(User user) {
        PackageDao dao = new PackageDao();
        return dao.getPackages(user);
    }

    public Package getPackage(int id) {
        PackageDao dao = new PackageDao();
        return dao.getPackage(id);
    }
}
