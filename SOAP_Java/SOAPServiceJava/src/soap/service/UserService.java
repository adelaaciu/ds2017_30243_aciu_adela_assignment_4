package soap.service;

import soap.dao.UserDao;
import soap.entities.User;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style= SOAPBinding.Style.RPC)
public class UserService implements IUserService{
    public String createUser(User user) {
        UserDao dao = new UserDao();
        return dao.createUser(user);
    }

    public User login(String name, String password) {
        UserDao dao =  new UserDao();
        return  dao.getUser(name,password);

    }
}
