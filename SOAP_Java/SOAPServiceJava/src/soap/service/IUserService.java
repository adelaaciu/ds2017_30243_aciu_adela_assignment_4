package soap.service;

import soap.entities.User;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style= SOAPBinding.Style.RPC)
public interface IUserService {
    String createUser(User user);

    User login(String name, String password);
}
