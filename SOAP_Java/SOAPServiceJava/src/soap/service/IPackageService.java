package soap.service;

import soap.entities.Package;
import soap.entities.PackageContainer;
import soap.entities.User;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style= SOAPBinding.Style.RPC)
public interface IPackageService {
    PackageContainer getPackages(User user);

    Package getPackage(int id);
}
