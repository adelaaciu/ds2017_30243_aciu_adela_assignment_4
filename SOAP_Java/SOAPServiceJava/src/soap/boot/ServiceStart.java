package soap.boot;

import soap.entities.Package;
import soap.entities.PackageContainer;
import soap.entities.PackageHalt;
import soap.entities.User;
import soap.service.IPackageService;
import soap.service.IUserService;
import soap.service.PackageService;
import soap.service.UserService;

import javax.xml.ws.Endpoint;
import java.util.List;

public class ServiceStart {
    public static void main(String[] args) {
        IUserService userService = new UserService();
        IPackageService packageService = new PackageService();

        String url1 = "http://localhost:1212/userJava";
        Endpoint.publish(url1, userService);
        System.out.println("Service started @ " + url1);

        String url2 = "http://localhost:1212/packageServiceJava";
        Endpoint.publish(url2, packageService);
        System.out.println("Service started @ " + url2);
    }
}
