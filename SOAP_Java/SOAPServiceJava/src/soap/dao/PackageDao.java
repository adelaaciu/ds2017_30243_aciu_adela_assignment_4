package soap.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import soap.entities.*;
import soap.entities.Package;
import soap.util.Hibernate;

import java.util.ArrayList;
import java.util.List;

public class PackageDao {

    public PackageContainer getPackages(User user) {
        Session session = Hibernate.openSession();
        Transaction tx = null;
        PackageContainer packageContainer = new PackageContainer();
        List<Package> packageList = new ArrayList<>();
        try {
            tx = session.getTransaction();
            tx.begin();

            String sql = "from Package where sender.id=" + user.getId() + "";
            Query query = session.createQuery(sql);
            packageList = (ArrayList<Package>) query.getResultList();
            tx.commit();

            for (Package p : packageList) {
                List<Halt> haltList = viewPackageRoute(p.getId());
                p.setRoute(haltList);
            }

        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            session.close();
        } finally {
            session.close();
        }

        packageContainer.setPackageList(packageList);
        return packageContainer;
    }

    public Package getPackage(int id) {
        Session session = Hibernate.openSession();
        Transaction tx = null;
        Package pack = new Package();
        try {
            tx = session.getTransaction();
            tx.begin();

            String sql = "from Package where id=" + id;
            Query query = session.createQuery(sql);
            pack = (Package) query.uniqueResult();
            tx.commit();

            List<Halt> haltList = viewPackageRoute(id);
            pack.setRoute(haltList);


        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return pack;
    }


    public List<Halt> viewPackageRoute(int id) {
        Session session = Hibernate.openSession();
        Transaction tx = null;
        List<Halt> haltList = new ArrayList<>();
        try {
            tx = session.getTransaction();
            tx.begin();
            String sql = "from PackageHalt where PackageId=" + id;
            Query query = session.createQuery(sql);
            ArrayList<PackageHalt> list = (ArrayList<PackageHalt>) query.getResultList();
            tx.commit();
            haltList = getHaltList(list);


        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return haltList;
    }

    private List<Halt> getHaltList(ArrayList<PackageHalt> list) {
        Session session = Hibernate.openSession();
        Transaction tx = null;
        List<Halt> haltList = new ArrayList<>();
        try {
            tx = session.getTransaction();
            tx.begin();

            for (PackageHalt p : list) {
                int routeId = p.getRouteId();
                String selectHalt = "from Halt where id=" + routeId;
                Query query = session.createQuery(selectHalt);
                Halt halt = (Halt) query.uniqueResult();
                haltList.add(halt);
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return haltList;
    }
}
