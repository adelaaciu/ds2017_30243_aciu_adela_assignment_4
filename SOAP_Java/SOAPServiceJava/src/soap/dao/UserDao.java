package soap.dao;

import org.hibernate.*;
import org.hibernate.query.Query;
import soap.entities.User;
import soap.util.Hibernate;

public class UserDao {

    public String createUser(User user) {
        Session session = Hibernate.openSession();
        Transaction tx = null;

        String status;
        try {
            tx = session.getTransaction();
            tx.begin();

            User userDb = getUser(user.getName());
            if (userDb == null) {
                user.setRole("user");
                session.save(user);
                status = "Created";
            } else {
                status = "Exist";
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            status = "Error";
        } finally {
            session.close();
        }
        return status;

    }

    private User getUser(String userName) {
        Session session = Hibernate.openSession();
        Transaction tx = null;
        User user = new User();
        try  {
            tx = session.getTransaction();
            tx = session.getTransaction();
            tx.begin();

            String sql = "from User where name='" + userName + "'";
            Query query = session.createQuery(sql);
            user = (User) query.uniqueResult();

            tx.commit();
            session.close();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            session.close();
        }
        return user;
    }

    public User getUser(String name, String password) {
        Session session = Hibernate.openSession();
        Transaction tx = null;
        User user = null;
        try  {
            tx = session.getTransaction();
            tx.begin();

            String sql = "from User where name='" + name + "'" + " and password='" + password + "'";
            Query query = session.createQuery(sql);
            user = (User) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return user;
    }
}
