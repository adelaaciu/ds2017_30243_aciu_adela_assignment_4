﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;

using MySql.Data;
using MySql.Data.MySqlClient;
using SOAPServiceNet.soap.entities;
using System.Globalization;

namespace SOAPServiceNet
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PackageService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PackageService.svc or PackageService.svc.cs at the Solution Explorer and start debugging.
    public class PackageService : IPackageService
    {
        
        MySql.Data.MySqlClient.MySqlConnection conn;
        string myConnectionString = "server=127.0.0.1;uid=root;" + "pwd=29zkq9;database=soap";

        public void addPackage(int senderId, int recieverId, string name, string description, string senderCity, string destinationCity)
        {
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();

                String insertStatement =
                 "INSERT INTO package"+
                 "("+
                 "`description`,"+
                 "`destinationCity`,"+
                 "`name`,"+
                 "`senderCity`,"+
                 "`tracking`,"+
                 "`reciever_id`,"+
                 "`sender_id`)"+
                 "VALUES"+
                 "  ('"+
                        description + "','" +
                        destinationCity + "','" +
                        name + "','" +
                        senderCity + "'," +
                        false + "," +
                        recieverId +"," +
                        senderId +");";


                MySqlCommand cmd = new MySqlCommand(insertStatement, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.Write(ex);
                conn.Close();
            }
        }

        public void addRoute(int packageId, string city, string time)
        { int haltId = 0;
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();


                DateTime date = DateTime.ParseExact(time, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                string formattedDate = date.ToString("yyyy-MM-dd HH:mm:ss");
               
                String querryHalt = "INSERT INTO halt " +
                                "( " + 
                               "`city`, " +
                               "`date`) " +
                               "VALUES" + 
                               "('" +
                               city + "','" +
                              time + "');";


                MySqlCommand cmd = new MySqlCommand(querryHalt, conn);
                cmd.ExecuteNonQuery();
                conn.Close();

                conn.Open();
                String getHalt = "SELECT id FROM halt where  city ='" + city + "'" + "and date='" + time+"'";
                cmd = new MySqlCommand(getHalt, conn);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    haltId = Int32.Parse(reader["id"].ToString());
                }
                conn.Close();
                conn.Open();
                String querryPachake_Halt = "INSERT INTO package_halt" +
                                             "(`PackageId`," +
                                             "`routeId`)" +
                                             "VALUES" +
                                             "(" + packageId +
                                             "," +
                                             haltId +
                                             ");";


                cmd = new MySqlCommand(querryPachake_Halt, conn);
                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.Write(ex);
                conn.Close();
            }
        }

        public void setTracking(int id, Boolean track)
        {
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();

                String updateStatement ="UPDATE package SET tracking=" + track + " where id=" + id;
                MySqlCommand cmd = new MySqlCommand(updateStatement, conn);
                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.Write(ex);
                conn.Close();
            }
        }

        public List<Halt> viewPackageRoute(int id)
        {
            List<Halt> haltList = new List<Halt>();
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT * FROM package_halt where PackageId=" + id, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                List<int> ids = new List<int>();

                while (reader.Read())
                {
                    ids.Add(Int32.Parse((reader["routeId"]).ToString()));
                }

                conn.Close();
                foreach (int i in ids)
                {
                    conn.Open();
                    cmd = new MySqlCommand("SELECT * FROM halt where id=" + i, conn);
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Halt h = new Halt();
                        h.City = (reader["city"]).ToString();
                        h.Date = DateTime.ParseExact((reader["date"]).ToString(), "MM/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                        haltList.Add(h);
                    }

                    conn.Close();
                }
              
            }
            catch (MySqlException ex)
            {
                Console.Write(ex);
                conn.Close();
            }

            return haltList;
        }

        void IPackageService.removePackage(int id)
        {
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();

                String updateStatement = "DELETE FROM package where id=" + id;
                MySqlCommand cmd = new MySqlCommand(updateStatement, conn);
                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.Write(ex);
                conn.Close();
            }
        }
    }
}
