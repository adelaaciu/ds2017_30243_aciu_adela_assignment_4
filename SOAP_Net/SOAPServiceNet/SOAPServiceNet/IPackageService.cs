﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SOAPServiceNet.soap.entities;

namespace SOAPServiceNet
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPackageService" in both code and config file together.
    [ServiceContract]
    public interface IPackageService
    {
        [OperationContract]
        void addPackage(int senderId, int recieverId, String name, String description, String senderCity, String destinationCity);

        [OperationContract]
        void setTracking(int id, Boolean track);

        [OperationContract]
        void addRoute(int id, String city, String time);

        [OperationContract]
        void removePackage(int id);
        
        [OperationContract]
        List<Halt> viewPackageRoute(int id);
    }
}
