﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOAPServiceNet.soap.entities
{
    public class Halt
    {
        public int Id { get; set; }
        public String City { get; set; }
        public DateTime Date { get; set; }
    }
}