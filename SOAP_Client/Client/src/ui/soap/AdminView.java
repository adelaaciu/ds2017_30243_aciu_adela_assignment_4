package ui.soap;

import net.soap.ArrayOfHalt;
import net.soap.Halt;
import net.soap.IPackageService;
import net.soap.PackageService;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class AdminView {

    private JFrame frame;
    private JTextField cityTextField;
    private JTextField dateTextField;
    private JTextField senderTF;
    private JTextField reveiverTF;
    private JTextField nameTf;
    private JTextField descriptionTf;
    private JTextField packageIdTf;
    private JTextField senderCity;
    private JTextField destinationCity;
    private JTextField packIdTf;
    private JTable table;
    String[] columnNames = {"City", "Time"};
    private String[][] data = {};
    private JTable table_1;
    private JTextField packageIdForTbl;
    private JButton btnView;
    private JButton btnAddInRoute;
    private JButton btnRegisterPackageTraking;
    private JButton btnAddPackage;
    private JRadioButton rdbtnTrack;
    private JButton btnRemovePackage;

    /**
     * Create the application.
     */
    public AdminView() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 755, 483);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblSenderId = new JLabel("Sender ID");
        lblSenderId.setBounds(10, 17, 78, 14);
        frame.getContentPane().add(lblSenderId);

        JLabel lblReceiverId = new JLabel("Receiver ID");
        lblReceiverId.setBounds(10, 42, 78, 14);
        frame.getContentPane().add(lblReceiverId);

        JLabel lblName = new JLabel("Name");
        lblName.setBounds(10, 67, 78, 14);
        frame.getContentPane().add(lblName);

        JLabel lblDescription = new JLabel("Description");
        lblDescription.setBounds(10, 96, 78, 14);
        frame.getContentPane().add(lblDescription);

        JLabel lblSenderCity = new JLabel("Sender City");
        lblSenderCity.setBounds(10, 121, 78, 14);
        frame.getContentPane().add(lblSenderCity);

        JLabel lblDestinationCity = new JLabel("Destination City");
        lblDestinationCity.setBounds(10, 146, 78, 14);
        frame.getContentPane().add(lblDestinationCity);

        btnAddPackage = new JButton("Add Package");
        btnAddPackage.setBounds(46, 189, 106, 23);
        frame.getContentPane().add(btnAddPackage);

        btnRegisterPackageTraking = new JButton("Register package for traking");
        btnRegisterPackageTraking.setBounds(264, 89, 202, 23);
        frame.getContentPane().add(btnRegisterPackageTraking);

        JLabel lblPackageId = new JLabel("Package ID");
        lblPackageId.setBounds(264, 14, 78, 14);
        frame.getContentPane().add(lblPackageId);

        JLabel lblPackageStatus = new JLabel("Package status");
        lblPackageStatus.setBounds(317, 146, 137, 14);
        frame.getContentPane().add(lblPackageStatus);

        JLabel lblTracking = new JLabel("Tracking");
        lblTracking.setBounds(264, 39, 78, 14);
        frame.getContentPane().add(lblTracking);

        JLabel label_1 = new JLabel("Package ID");
        label_1.setBounds(264, 168, 78, 14);
        frame.getContentPane().add(label_1);

        JLabel lblCity = new JLabel("City");
        lblCity.setBounds(264, 193, 46, 14);
        frame.getContentPane().add(lblCity);

        JLabel lblTime = new JLabel("Time");
        lblTime.setBounds(264, 218, 46, 14);
        frame.getContentPane().add(lblTime);

        cityTextField = new JTextField();
        cityTextField.setBounds(368, 190, 86, 20);
        frame.getContentPane().add(cityTextField);
        cityTextField.setColumns(10);

        dateTextField = new JTextField();
        dateTextField.setBounds(368, 215, 86, 20);
        frame.getContentPane().add(dateTextField);
        dateTextField.setColumns(10);

        senderTF = new JTextField();
        senderTF.setBounds(106, 14, 86, 20);
        frame.getContentPane().add(senderTF);
        senderTF.setColumns(10);

        reveiverTF = new JTextField();
        reveiverTF.setBounds(106, 39, 86, 20);
        frame.getContentPane().add(reveiverTF);
        reveiverTF.setColumns(10);

        nameTf = new JTextField();
        nameTf.setBounds(106, 64, 86, 20);
        frame.getContentPane().add(nameTf);
        nameTf.setColumns(10);

        descriptionTf = new JTextField();
        descriptionTf.setBounds(106, 93, 86, 20);
        frame.getContentPane().add(descriptionTf);
        descriptionTf.setColumns(10);

        packageIdTf = new JTextField();
        packageIdTf.setBounds(342, 11, 86, 20);
        frame.getContentPane().add(packageIdTf);
        packageIdTf.setColumns(10);

        senderCity = new JTextField();
        senderCity.setBounds(106, 118, 86, 20);
        frame.getContentPane().add(senderCity);
        senderCity.setColumns(10);

        destinationCity = new JTextField();
        destinationCity.setBounds(106, 143, 86, 20);
        frame.getContentPane().add(destinationCity);
        destinationCity.setColumns(10);

        btnAddInRoute = new JButton("Add in route");
        btnAddInRoute.setBounds(475, 189, 136, 23);

        frame.getContentPane().add(btnAddInRoute);

        rdbtnTrack = new JRadioButton("track");
        rdbtnTrack.setBounds(342, 38, 109, 23);
        frame.getContentPane().add(rdbtnTrack);

        packIdTf = new JTextField();
        packIdTf.setBounds(368, 165, 86, 20);
        frame.getContentPane().add(packIdTf);
        packIdTf.setColumns(10);

        JPanel panel = new JPanel();
        panel.setBounds(10, 287, 639, 146);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        TableModel model = new DefaultTableModel(data, columnNames);
        table_1 = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table_1);
        scrollPane.setBounds(10, 11, 619, 124);
        panel.add(scrollPane, BorderLayout.CENTER);

        packageIdForTbl = new JTextField();
        packageIdForTbl.setColumns(10);
        packageIdForTbl.setBounds(103, 249, 86, 20);
        frame.getContentPane().add(packageIdForTbl);

        JLabel label = new JLabel("Package ID");
        label.setBounds(25, 252, 78, 14);
        frame.getContentPane().add(label);

        btnView = new JButton("View route");
        btnView.setBounds(205, 246, 105, 23);
        frame.getContentPane().add(btnView);

        btnRemovePackage = new JButton("Remove Package");
        btnRemovePackage.setBounds(487, 13, 89, 23);
        frame.getContentPane().add(btnRemovePackage);

        addListeners();
    }

    private void addListeners() {
        btnView.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int id = Integer.valueOf(packageIdForTbl.getText());

                // get route list for package with id and set model to table
                IPackageService service = new PackageService().getBasicHttpBindingIPackageService();
                ArrayOfHalt list = service.viewPackageRoute(id);


                int index = 0;
                if(list.getHalt() != null) {
                    data = new String[list.getHalt().size()][2];
                    for (Halt x : list.getHalt()) {
                        data[index][0] = x.getCity().getValue();
                        data[index][1] = x.getDate().toString();
                        index++;
                    }
                }

                TableModel model = new DefaultTableModel(data, columnNames);
                table_1.setModel(model);

            }
        });

        btnAddInRoute.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int id = Integer.valueOf(packIdTf.getText());
                String city = cityTextField.getText();
                String date = dateTextField.getText();

                IPackageService service = new PackageService().getBasicHttpBindingIPackageService();
                service.addRoute(id, city, date);
            }
        });

        btnRegisterPackageTraking.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int packageId = Integer.valueOf(packageIdTf.getText());
                boolean tracking = rdbtnTrack.isSelected();

                // update package
                IPackageService service = new PackageService().getBasicHttpBindingIPackageService();
                service.setTracking(packageId, tracking);
            }
        });

        btnAddPackage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int senderId = Integer.valueOf(senderTF.getText());
                int receiverId = Integer.valueOf(reveiverTF.getText());
                String name = nameTf.getText();
                String description = descriptionTf.getText();
                String senderC = senderCity.getText();
                String destinationC = destinationCity.getText();

                // create package
                IPackageService service = new PackageService().getBasicHttpBindingIPackageService();
                service.addPackage(senderId, receiverId, name, description, senderC, destinationC);
            }
        });

        btnRemovePackage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int packageId = Integer.valueOf(packageIdTf.getText());
                // met remove
                IPackageService service = new PackageService().getBasicHttpBindingIPackageService();
                service.removePackage(packageId);
            }
        });

    }

    public JFrame getFrame() {
        return frame;
    }

}
