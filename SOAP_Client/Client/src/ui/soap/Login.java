package ui.soap;

import soap.java.user.User;
import soap.java.user.UserService;
import soap.java.user.UserServiceService;

import java.awt.EventQueue;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private JTextField usernameTextField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = usernameTextField.getText();
				String password = String.valueOf(passwordField.getPassword());
				
				//apelare metoda de login
				UserService service = new UserServiceService().getUserServicePort();
				User user = service.login(username,password);

				if(null != user.getName() || !user.getName().isEmpty()) {
					if (user.getRole().equals("admin")) {
						AdminView adminView = new AdminView();
						adminView.getFrame().setVisible(true);
					} else {
						ClientView clientView = new ClientView(user);
						clientView.getFrame().setVisible(true);
					}
					frame.dispose();
				}

			}
		});
		btnLogin.setBounds(98, 191, 89, 23);
		frame.getContentPane().add(btnLogin);
		
		usernameTextField = new JTextField();
		usernameTextField.setBounds(215, 77, 86, 20);
		frame.getContentPane().add(usernameTextField);
		usernameTextField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(215, 122, 86, 23);
		frame.getContentPane().add(passwordField);
		
		JLabel lblUsername = new JLabel("username:");
		lblUsername.setBounds(116, 79, 89, 17);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("password");
		lblPassword.setBounds(116, 124, 89, 19);
		frame.getContentPane().add(lblPassword);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setBounds(230, 191, 89, 23);
		btnRegister.addActionListener(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String username = usernameTextField.getText();
				String password = String.valueOf(passwordField.getPassword());

				//apelare metoda de login
				UserService service = new UserServiceService().getUserServicePort();
				User u  = new User();
				u.setName(username);
				u.setPassword(password);
				String s = service.createUser(u);
				System.out.println(s);
			}
		});
		frame.getContentPane().add(btnRegister);
	}
}
