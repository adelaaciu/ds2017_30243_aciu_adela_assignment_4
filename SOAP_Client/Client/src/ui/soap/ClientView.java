package ui.soap;

import sap.java.pachete.*;
import sap.java.pachete.Package;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class ClientView {

    private JFrame frame;
    private JTextField textField;
    private JButton btnSearch;
    private JButton btnListallpackages;
    String[] columnNames = {"Sender", "Receiver", "Name", "Description", "Sender City", "Destination city", "Route"};
    private String[][] data = {};
    private JTable table_1;
    private User user;

    /**
     * Create the application.
     */
    public ClientView(soap.java.user.User user) {
        this.user = new User();
        this.user.setId(user.getId());
        this.user.setRole(user.getRole());
        this.user.setName(user.getName());
        this.user.setPassword(user.getPassword());
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 641, 433);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(10, 128, 605, 196);
        TableModel model = new DefaultTableModel(data, columnNames);
        table_1 = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table_1);
        scrollPane.setBounds(10, 11, 619, 124);
        panel.add(scrollPane, BorderLayout.CENTER);

        frame.getContentPane().add(panel);

        textField = new JTextField();
        textField.setBounds(88, 18, 86, 20);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblNewLabel = new JLabel("Package id");
        lblNewLabel.setBounds(20, 21, 73, 14);
        frame.getContentPane().add(lblNewLabel);

        btnSearch = new JButton("Search");

        btnSearch.setBounds(202, 17, 89, 23);
        frame.getContentPane().add(btnSearch);

        btnListallpackages = new JButton("ListAllPackages");
        btnListallpackages.setBounds(10, 73, 164, 23);

        addListeners();
        frame.getContentPane().add(btnListallpackages);
    }

    private void addListeners() {
        btnSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(textField.getText());
                PackageService service = new PackageServiceService().getPackageServicePort();
                Package aPackage = service.getPackage(id);
                data = new String[1][7];
                if (null != aPackage || !aPackage.getName().isEmpty()) {
                    data[0][0] = aPackage.getSender().getName();
                    data[0][1] = aPackage.getReciever().getName();
                    data[0][2] = aPackage.getName();
                    data[0][3] = aPackage.getDescription();
                    data[0][4] = aPackage.getSenderCity();
                    data[0][5] = aPackage.getDestinationCity();
                    if (aPackage.getRoute().size() > 0) {
                        Halt h = aPackage.getRoute().get(aPackage.getRoute().size() - 1);
                        data[0][6] = h.getCity() + " " + h.getDate();
                    } else {
                        data[0][6] = "";
                    }

                    TableModel model = new DefaultTableModel(data, columnNames);
                    table_1.setModel(model);
                }

            }

        });

        btnListallpackages.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // get data and populate table
                PackageService service = new PackageServiceService().getPackageServicePort();
                PackageContainer packageContainer = service.getPackages(user);

                int index = 0;
                if (packageContainer.getPackageList() != null) {
                    data = new String[packageContainer.getPackageList().size()][7];
                    for (Package aPackage : packageContainer.getPackageList()) {
                        data[index][0] = aPackage.getSender().getName();
                        data[index][1] = aPackage.getReciever().getName();
                        data[index][2] = aPackage.getName();
                        data[index][3] = aPackage.getDescription();
                        data[index][4] = aPackage.getSenderCity();
                        data[index][5] = aPackage.getDestinationCity();
                        Halt h = aPackage.getRoute().get(aPackage.getRoute().size() - 1);
                        data[index][6] = h.getCity() + " " + h.getDate();
                        index++;
                    }

                    TableModel model = new DefaultTableModel(data, columnNames);
                    table_1.setModel(model);
                }




            }
        });
    }

    public JFrame getFrame() {
        return frame;
    }

}
