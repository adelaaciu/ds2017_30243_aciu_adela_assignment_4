
package net.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="viewPackageRouteResult" type="{http://schemas.datacontract.org/2004/07/SOAPServiceNet.soap.entities}ArrayOfHalt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "viewPackageRouteResult"
})
@XmlRootElement(name = "viewPackageRouteResponse")
public class ViewPackageRouteResponse {

    @XmlElementRef(name = "viewPackageRouteResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfHalt> viewPackageRouteResult;

    /**
     * Gets the value of the viewPackageRouteResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfHalt }{@code >}
     *     
     */
    public JAXBElement<ArrayOfHalt> getViewPackageRouteResult() {
        return viewPackageRouteResult;
    }

    /**
     * Sets the value of the viewPackageRouteResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfHalt }{@code >}
     *     
     */
    public void setViewPackageRouteResult(JAXBElement<ArrayOfHalt> value) {
        this.viewPackageRouteResult = value;
    }

}
