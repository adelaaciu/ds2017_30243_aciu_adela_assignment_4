
package net.soap;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfHalt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfHalt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Halt" type="{http://schemas.datacontract.org/2004/07/SOAPServiceNet.soap.entities}Halt" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfHalt", namespace = "http://schemas.datacontract.org/2004/07/SOAPServiceNet.soap.entities", propOrder = {
    "halt"
})
public class ArrayOfHalt {

    @XmlElement(name = "Halt", nillable = true)
    protected List<Halt> halt;

    /**
     * Gets the value of the halt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the halt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHalt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Halt }
     * 
     * 
     */
    public List<Halt> getHalt() {
        if (halt == null) {
            halt = new ArrayList<Halt>();
        }
        return this.halt;
    }

}
